CI_COMMIT_SHA ?= $(shell git log -n 1 --pretty=format:"%H")
VERSION ?= $(shell cat version.txt)

check-vars:
ifeq ($(CI_COMMIT_SHA),)
$(error CI_COMMIT_SHA is empty)
endif

ifeq ($(VERSION),)
$(error VERSION is empty)
endif

compile: check-vars
	mkdir -p build/
	go build -v -ldflags "-X main.gitCommit=$(CI_COMMIT_SHA) -X main.version=$(VERSION)" -o build/app main.go
