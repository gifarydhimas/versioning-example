package main

import "fmt"

var (
	version   string
	gitCommit string
)

func main() {
	fmt.Printf("Version: %s\nGit Commit: %s\n", version, gitCommit)
}
